

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex2 {
    public static void main(String[] args) {
        new GUI();
    }
} // main

class GUI extends JFrame {
    JLabel jLabel1 = new JLabel("Text1");
    JTextField text1 = new JTextField();

    JLabel jLabel2 = new JLabel("Text2");
    JTextField text2 = new JTextField();
    JButton jButton = new JButton("Calculeaza");
    JLabel jLabel3 = new JLabel("Suma");
    JTextField summ = new JTextField();
    

    GUI() {
        this.setTitle("Exercitiul 2");
        this.setSize(500, 300);
        init();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        jLabel1.setBounds(20, 20, 80, 20);
        text1.setBounds(100, 20, 350, 20);


        jLabel2.setBounds(20, 50, 80, 20);
        text2.setBounds(100, 50, 350, 20);

        jButton.setBounds(20, 80, 100, 20);
        jButton.addActionListener(new TratareButon());
        
        jLabel3.setBounds(20, 110, 80, 20);
        summ.setBounds(100, 110, 80, 20);

        this.add(jLabel1);
        this.add(jLabel2);
        
        this.add(text1);
        this.add(text2);
        this.add(jLabel3);
        this.add(jButton);

        this.add(summ);
    }

    class TratareButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String words = text1.getText();
            String words2=text2.getText();
            int nrWords = words.split("\\s+", 0).length;
            int nrWords2=words2.split("\\s+",0).length;
            int nrChars = 0;
            int nrChars2=0;
            for (int i = 0; i < words.length(); i++) {
                if (words.charAt(i) != ' ') {
                    nrChars++;
                }
            }
            for(int j=0;j<words2.length();j++) {
            	if(words2.charAt(j)!=' ')
            		nrChars2++;
            }
            summ.setText(nrChars+nrChars2+"");
        }
    }
}

